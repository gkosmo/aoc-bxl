class PagesController < ApplicationController
  skip_before_action :authenticate_user!, only: [ :home ]

  def home
    @day = Day.find_by(date: Date.today)
    @new_problem = Problem.new
  end


end
