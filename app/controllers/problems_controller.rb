class ProblemsController < ApplicationController
  
  def show
    @problem = Problem.find(params[:id])
    if params[:tested_solution].present?
      @tested_solution = TestedSolution.new(input: params[:tested_solution][:input], answer: Solution.find(params[:tested_solution][:answer_id]))
      input = @tested_solution.input
      eval("def solution(input)\n" + self.code + "\n end")
      @tested_solution.output = solution(input)
    end
    @solution = Solution.new
  end

  def create
    @problem = Problem.new(problem_params)
    
    @problem.save
    redirect_to root_path, notice: "Saved"
  end

  private

  def problem_params
    params.require(:problem).permit(:description, :test_input, :test_result, :position, :day_id)
  end
end
