class SolutionsController < ApplicationController


  def create
    @solution = Solution.new(solution_params)
    @solution.code = @solution.code
    @solution.user = current_user
    @problem = @solution.problem
    if @solution.save
      redirect_to problem_path(@solution.problem)
    else 
      render 'problems/show'
    end
  end
  def destroy 
    @solution = Solution.find(params[:id])
    @solution.destroy
    redirect_to problem_path(@solution.problem)
  end
  
  private 

  def solution_params
    params.require(:solution).permit(:code, :comment, :language, :problem_id)
  end
end
