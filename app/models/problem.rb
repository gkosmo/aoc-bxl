class Problem < ApplicationRecord
  belongs_to :day
  has_many :solutions
end
