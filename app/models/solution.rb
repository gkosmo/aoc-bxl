class Solution < ApplicationRecord
  belongs_to :problem
  belongs_to :user

  validates :language, presence: true 
  validates :code, presence: true
  validate :code_works, on: :create

  private

  def code_works
    if self.language.downcase == "ruby"
      begin
        input = problem.test_input
        result = eval("def solution(input)\n" + self.code + "\n end")
        p solution(input)
        errors.add(:code, "doesn't pass") unless solution(input).to_i == problem.test_result.to_i
      rescue
        errors.add(:code, "doesn't work")
      end
    end 
  end
end
