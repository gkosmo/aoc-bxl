class Day < ApplicationRecord
  has_many :problems
  has_many :solutions, through: :problems
  validates :date, uniqueness: true
end
