class TestedSolution
  attr_accessor :input
  attr_accessor :answer
  attr_accessor :output

  
  def initialize(opt = {})
    @input = opt[:input]
    @answer = opt[:answer]
    @output = opt[:output]
  end
end