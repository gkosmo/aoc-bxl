import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "toggable" ]

  connect() {
     console.log("toggle")
    }

    toggle(event) {
        this.toggableTargets.forEach(target => target.classList.toggle("d-none"))
     }

}
