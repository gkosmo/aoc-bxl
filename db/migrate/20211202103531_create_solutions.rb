class CreateSolutions < ActiveRecord::Migration[6.1]
  def change
    create_table :solutions do |t|
      t.references :problem, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.string :language
      t.text :code
      t.string :comment

      t.timestamps
    end
  end
end
