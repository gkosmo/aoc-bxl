class CreateProblems < ActiveRecord::Migration[6.1]
  def change
    create_table :problems do |t|
      t.references :day, null: false, foreign_key: true
      t.text :test_input
      t.string :test_result
      t.integer :position
      t.text :description

      t.timestamps
    end
  end
end
